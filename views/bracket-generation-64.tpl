<!DOCTYPE html>
<html>
<title>64 bracket generation</title>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<body>
<h3>1. Copy the following code block:</h3>
<pre>
<code>
function myFunction() {
    var form = FormApp.create('{{bracketName}} Bracket | Round 1 of 2');
    form.setDescription('Vote with your heart, from your own personal pure enjoyment. Disregard community consensus and popular opinion. It is more fun that way. All polls are optional, so only vote what you are familiar with.');
    form.setLimitOneResponsePerUser(true);
    form.setAllowResponseEdits(false);
    form.setPublishingSummary(false);

    form.addMultipleChoiceItem()
    .setTitle('{{option1}} vs {{option64}}')
    .setChoiceValues(['{{option1}}','{{option64}}'])
    .setRequired(false);

    form.addMultipleChoiceItem()
    .setTitle('{{option32}} vs {{option33}}')
    .setChoiceValues(['{{option32}}','{{option33}}'])
    .setRequired(false);

    form.addMultipleChoiceItem()
    .setTitle('{{option16}} vs {{option49}}')
    .setChoiceValues(['{{option16}}','{{option49}}'])
    .setRequired(false);

    form.addMultipleChoiceItem()
    .setTitle('{{option17}} vs {{option48}}')
    .setChoiceValues(['{{option17}}','{{option48}}'])
    .setRequired(false);

    form.addMultipleChoiceItem()
    .setTitle('{{option8}} vs {{option57}}')
    .setChoiceValues(['{{option8}}','{{option57}}'])
    .setRequired(false);

    form.addMultipleChoiceItem()
    .setTitle('{{option25}} vs {{option40}}')
    .setChoiceValues(['{{option25}}','{{option40}}'])
    .setRequired(false);

    form.addMultipleChoiceItem()
    .setTitle('{{option9}} vs {{option56}}')
    .setChoiceValues(['{{option9}}','{{option56}}'])
    .setRequired(false);

    form.addMultipleChoiceItem()
    .setTitle('{{option24}} vs {{option41}}')
    .setChoiceValues(['{{option24}}','{{option41}}'])
    .setRequired(false);

    form.addMultipleChoiceItem()
    .setTitle('{{option4}} vs {{option61}}')
    .setChoiceValues(['{{option4}}','{{option61}}'])
    .setRequired(false);

    form.addMultipleChoiceItem()
    .setTitle('{{option29}} vs {{option36}}')
    .setChoiceValues(['{{option29}}','{{option36}}'])
    .setRequired(false);

    form.addMultipleChoiceItem()
    .setTitle('{{option13}} vs {{option52}}')
    .setChoiceValues(['{{option13}}','{{option52}}'])
    .setRequired(false);

    form.addMultipleChoiceItem()
    .setTitle('{{option20}} vs {{option45}}')
    .setChoiceValues(['{{option20}}','{{option45}}'])
    .setRequired(false);

    form.addMultipleChoiceItem()
    .setTitle('{{option5}} vs {{option60}}')
    .setChoiceValues(['{{option5}}','{{option60}}'])
    .setRequired(false);

    form.addMultipleChoiceItem()
    .setTitle('{{option28}} vs {{option37}}')
    .setChoiceValues(['{{option28}}','{{option37}}'])
    .setRequired(false);

    form.addMultipleChoiceItem()
    .setTitle('{{option12}} vs {{option53}}')
    .setChoiceValues(['{{option12}}','{{option53}}'])
    .setRequired(false);

    form.addMultipleChoiceItem()
    .setTitle('{{option21}} vs {{option44}}')
    .setChoiceValues(['{{option21}}','{{option44}}'])
    .setRequired(false);

    form.addMultipleChoiceItem()
    .setTitle('{{option2}} vs {{option63}}')
    .setChoiceValues(['{{option2}}','{{option63}}'])
    .setRequired(false);

    form.addMultipleChoiceItem()
    .setTitle('{{option31}} vs {{option34}}')
    .setChoiceValues(['{{option31}}','{{option34}}'])
    .setRequired(false);

    form.addMultipleChoiceItem()
    .setTitle('{{option15}} vs {{option50}}')
    .setChoiceValues(['{{option15}}','{{option50}}'])
    .setRequired(false);

    form.addMultipleChoiceItem()
    .setTitle('{{option18}} vs {{option47}}')
    .setChoiceValues(['{{option18}}','{{option47}}'])
    .setRequired(false);

    form.addMultipleChoiceItem()
    .setTitle('{{option7}} vs {{option58}}')
    .setChoiceValues(['{{option7}}','{{option58}}'])
    .setRequired(false);

    form.addMultipleChoiceItem()
    .setTitle('{{option26}} vs {{option39}}')
    .setChoiceValues(['{{option26}}','{{option39}}'])
    .setRequired(false);

    form.addMultipleChoiceItem()
    .setTitle('{{option10}} vs {{option55}}')
    .setChoiceValues(['{{option10}}','{{option55}}'])
    .setRequired(false);

    form.addMultipleChoiceItem()
    .setTitle('{{option23}} vs {{option42}}')
    .setChoiceValues(['{{option23}}','{{option42}}'])
    .setRequired(false);

    form.addMultipleChoiceItem()
    .setTitle('{{option3}} vs {{option62}}')
    .setChoiceValues(['{{option3}}','{{option62}}'])
    .setRequired(false);

    form.addMultipleChoiceItem()
    .setTitle('{{option30}} vs {{option35}}')
    .setChoiceValues(['{{option30}}','{{option35}}'])
    .setRequired(false);

    form.addMultipleChoiceItem()
    .setTitle('{{option14}} vs {{option51}}')
    .setChoiceValues(['{{option14}}','{{option51}}'])
    .setRequired(false);

    form.addMultipleChoiceItem()
    .setTitle('{{option19}} vs {{option46}}')
    .setChoiceValues(['{{option19}}','{{option46}}'])
    .setRequired(false);

    form.addMultipleChoiceItem()
    .setTitle('{{option6}} vs {{option59}}')
    .setChoiceValues(['{{option6}}','{{option59}}'])
    .setRequired(false);

    form.addMultipleChoiceItem()
    .setTitle('{{option27}} vs {{option38}}')
    .setChoiceValues(['{{option27}}','{{option38}}'])
    .setRequired(false);

    form.addMultipleChoiceItem()
    .setTitle('{{option11}} vs {{option54}}')
    .setChoiceValues(['{{option11}}','{{option54}}'])
    .setRequired(false);

    form.addMultipleChoiceItem()
    .setTitle('{{option22}} vs {{option43}}')
    .setChoiceValues(['{{option22}}','{{option43}}'])
    .setRequired(false);
}
</code>
</pre>
<h3>2. Go to the following link (make sure you are signed into the music_survivor google account):</h3>
<a href="https://script.google.com/home/my" target="_blank">https://script.google.com/home/my</a>
<h3>3. Open the "Bracket Creator Round (64)" script</h3>
<h3>4. Replace what is already in the script (the script is titled Code.gs) with the script you copied</h3>
<h3>5. Press the play button on the top toolbar to run the script</h3>
<h3>6. The 64 bracket google form should now be created, check Drive to see if it's there and make sure it looks right</h3>
<footer>
    <p><em>Get the source code for this website <a href="https://gitlab.com/KevinThomas0/music_survivor" target="_blank">here</a>.</em></p>
</footer>
</body>
</html>
